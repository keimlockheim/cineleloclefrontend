## Installation du projet

1. Tappez **npm install** afin de résoudre les dépendances du projet
2. Tappez **npm run dev** pour lancer le projet
---

## Divers

Le projet a été configuré en suivant les indications du lien suivant: https://medium.com/@mikeal/vue-js-electron-the-easy-way-adc3ca09234a
